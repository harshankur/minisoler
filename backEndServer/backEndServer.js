const express = require('express')
const fetch = require('node-fetch')
const fileUpload = require('express-fileupload');

const app = express()

app.use(fileUpload());

// Allow CORS
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

var bodyParser = require('body-parser')
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

app.get("/testServer", function (req, res) {
    res.setHeader('Content-Type', 'application/json');
	res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");

    var responseJSON = {
        message: "Received empty request from Client. Connect FrontEnd, BackEnd and Printing Services.",
    };

    res.send(responseJSON);
})

//Server listening at port 3000
app.listen(3000, () => console.log('Hello World! Example app listening on port 3000!'))